{%- if grains.get('provisioned', False) == False %}

{#- 1) We want to sync all custom modules we have. #}
sync_all:
  module.run:
    - name: saltutil.sync_all
    - refresh: True

{#- 2) We also want to refresh all pillars, which will refresh the LDAP pillar. #}
refresh_pillar:
  module.run:
    - name: saltutil.refresh_pillar
    - require:
      - module: sync_all

{#- 3) We also want to update the mine, so we can issue new certificates to this minion. #}
mine_update:
  module.run:
    - name: mine.update
    - require:
      - module: refresh_pillar

{#- 4) With everything in place for it to be successful, we want to run a highstate. #}
highstate_run:
  module.run:
    - name: state.apply
    - require:
      - module: mine_update

{#- 5) And cleanup the static environment #}
grains_cleanup_environment:
  # Purge the environment grain from the machine
  grains.absent:
    - name: environment
    - force: True
    - destructive: True
    - require:
      - module: highstate_run

{#- 6) And set the minion as provisioned. #}
grain_set_provisioned:
  grains.present:
    - name: provisioned
    - value: True
    - require:
      - grains: grains_cleanup_environment

{#- 7) And run any hooks configured to be executed after a successful provisioning  procedure. #}
{%- for hook, states in salt['pillar.get']('provisioning:hooks', {}).items() %}
provisioning-hook-{{ hook }}:
  {%- for state, options in states.items() %}
  {{ state }}:
    {%- for key, value in options.items() %}
    - {{ key }}: {{ value }}
    {%- endfor %}
    - onchanges:
      - grains: grain_set_provisioned
  {%- endfor %}
{% endfor %}

{%- endif %}
