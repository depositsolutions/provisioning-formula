# Provisioning formula

This formula is responsible for running provisioning procedures over a new machine.
The provisioning formula should be configured in the `startup_states` configuration
in the minion, as below:

```yaml
startup_states: sls
sls_list:
  - provisioning
```

The goal of this formula is to sync all grains, states, modules, and mine, then
apply a `highstate`onto the machine, marking it as *provisioned* through the
`provisioned` grain.

If the formula is executed again on a machine that is already provisioned, it
will do nothing. 

This formula must be the first formula to be executed by a new Salt minion
during its first boot sequence. 
